import express, { Request, Response } from 'express';
import endpoint from './config/endpoints.config';
import connectDatabase from './config/connectDatabase';
import { authRoute } from './routes/auth.route';

const app = express();
const PORT = endpoint.PORT;

connectDatabase();

app.use(express.json());
app.use('/api/auth', authRoute);

app.get('/', (req: Request, res: Response) => {
    res.send('<h1>This is server-side homepage.</h1>');
});

app.listen(PORT, () => {
    console.log(`Server is listening in port ${PORT}.`);
});
