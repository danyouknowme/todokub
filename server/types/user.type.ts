export interface UserProps {
    _id: String,
    username: String,
    password: String,
    firstname: String,
    lastname: String;
    isAdmin: Boolean;
}
