import CryptoJS from 'crypto-js';

const encryptPassword = (password: string, passwordSecretKey: string) => {
    const encryptPass = CryptoJS.AES.encrypt(password, passwordSecretKey).toString();
    return encryptPass;
};

const decryptPassword = (password: string, passwordSecretKey: string) => {
    const decryptPass = CryptoJS.AES.decrypt(password, passwordSecretKey).toString(CryptoJS.enc.Utf8);
    return decryptPass;
};

export { encryptPassword, decryptPassword };
