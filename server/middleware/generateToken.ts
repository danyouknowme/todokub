import * as jwt from 'jsonwebtoken';
import { UserProps } from '../types/user.type';


const generateToken = (user: UserProps, secretKey: jwt.Secret) => {
    const token = jwt.sign({
        id: user._id,
        isAdmin: user.isAdmin
    }, secretKey,
        { expiresIn: '3d' });
    return token;
};

export { generateToken };
