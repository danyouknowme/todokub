import { Request, Response } from 'express';
import { User } from '../models/user.model';
import endpoint from '../config/endpoints.config';
import { generateToken } from '../middleware/generateToken';
import { encryptPassword, decryptPassword } from '../middleware/password';

const register = async (req: Request, res: Response) => {
    const { username, password, firstname, lastname } = req.body;

    const userExists = await User.findOne({ username });
    if (userExists) {
        return res.status(400).json({ message: 'User already exists!' });
    }

    const newUser = new User({
        username,
        password: encryptPassword(password, endpoint.PASS_SEC),
        firstname,
        lastname
    });
    try {
        const savedUser = await newUser.save();
        return res.status(200).json(savedUser);
    } catch (error) {
        return res.status(500).json(error);
    }
};

const login = async (req: Request, res: Response) => {
    const { username, password } = req.body;

    try {
        const user = await User.findOne({ username });
        if (!user) {
            return res.status(401).json({ message: 'Username or Password is incorrect!' });
        }

        const originalPassword = decryptPassword(user.password, endpoint.PASS_SEC);
        if (originalPassword !== password) {
            return res.status(401).json({ message: 'You entered incorrect password' });
        }

        const accessToken = generateToken(user, endpoint.JWT_SEC);
        const { password: userPassword, ...others } = user._doc;
        res.status(200).json({ ...others, accessToken });
    } catch (error) {
        res.status(500).json(error);
    }
};

export { register, login };
