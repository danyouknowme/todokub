import mongoose from 'mongoose';

const { model, Schema } = mongoose;

const userSchema = new Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    isAdmin: { type: Boolean, default: false },
});

const User = model('User', userSchema);

export { User };
