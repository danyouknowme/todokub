import * as dotenv from 'dotenv';

dotenv.config();

export default {
    PORT: process.env.PORT || 5050,
    MONGO_URI: process.env.MONGO_URI || '',
    PASS_SEC: process.env.PASS_SEC || '',
    JWT_SEC: process.env.JWT_SEC || '',
};
