import mongoose from 'mongoose';
import endpoint from './endpoints.config';

const connectDatabase = async () => {
    try {
        await mongoose.connect(endpoint.MONGO_URI);
        console.log('Connected to database successfully.');
    } catch (error) {
        console.error(error);
        process.exit(1);
    }
};

export default connectDatabase;
